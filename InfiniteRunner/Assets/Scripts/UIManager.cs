﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text coinCounter;
    private int currentCoins;

    public Text crono;
    private float currentTime;

    public bool pausa;
    public GameObject pauseMenu;
    public GameObject continueButton;

    private PlayerMovement playerMov;

    private Parallax[] allParallaxes;
    private ParallaxNoFloor[] parallaxesNoFloor;

    void Start()
    {
        pausa = false;
        coinCounter.text = "0";
        currentCoins = 0;

        currentTime = 0;
        crono.text = "00:00";
        StartCoroutine(CronoCorountine());

        pauseMenu.SetActive(false);
        allParallaxes = FindObjectsOfType<Parallax>();
        parallaxesNoFloor = FindObjectsOfType<ParallaxNoFloor>();

        playerMov = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>(); ;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && pausa == false && playerMov.isPlayerOnTheFloor())
        {
            PauseGame();
        }
    }

    //Timer
    private IEnumerator CronoCorountine()
    {
        while (pausa == false)
        {
            yield return new WaitForSeconds(1.0f);
            currentTime++;
            ShowTimerWithFormat();
        }
    }

    private void ShowTimerWithFormat()
    {
        string minutes = Mathf.Floor(currentTime / 60).ToString("00");
        string seconds = (currentTime % 60).ToString("00");

        crono.text = string.Format("{0}:{1}", minutes, seconds);
    }

    //Coins
    public void AddCoin()
    {
        currentCoins++;
        coinCounter.text = currentCoins.ToString();
    }

    public void LoseCoins(int howManyCoins)
    {
        if (howManyCoins >= currentCoins)
        {
            currentCoins = 0;
        }
        else
        {
            currentCoins -= howManyCoins;
        }

        coinCounter.text = currentCoins.ToString();
    }

    //Pause and Endgame
    public void PauseGame()
    {
        pausa = true;

        StopAllParallaxes();
        playerMov.StopRunning();

        pauseMenu.SetActive(true);
        continueButton.SetActive(true);
    }

    public void EndGame()
    {
        pausa = true;
        StopAllParallaxes();
        playerMov.Die();
        //show Pausa Menu
        pauseMenu.SetActive(true);
        continueButton.SetActive(false);
    }

    public void Continue()
    {
        pausa = false;
        StartAllParallaxes();
        playerMov.ContinueRunning();
        pauseMenu.SetActive(false);
    }

    //DeadMenu Buttons
    public void RetryButton()
    {
        //Start again
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitButton()
    {
        //Go to main title
        SceneManager.LoadScene(0);
    }

    //Parallaxes control
    public void StopAllParallaxes()
    {
        //if more parallaxes are added, I should find a way to combine them.

        for (int i = 0; i < allParallaxes.Length; i++)
        {
            allParallaxes[i].StopParallax();
        }

        for (int j = 0; j < parallaxesNoFloor.Length; j++)
        {
            parallaxesNoFloor[j].StopParallax();
        }
    }
    public void StartAllParallaxes()
    {
        for (int i = 0; i < allParallaxes.Length; i++)
        {
            allParallaxes[i].StartParallax();
        }

        for (int j = 0; j < parallaxesNoFloor.Length; j++)
        {
            parallaxesNoFloor[j].StartParallax();
        }
    }

    //References to save Score
    public float GetCurrentTime()
    {
        return currentTime;
    }

    public int GetCurrentCoins()
    {
        return currentCoins;
    }
}
