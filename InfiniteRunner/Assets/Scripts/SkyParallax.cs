﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyParallax : MonoBehaviour
{
    [SerializeField] private float parallaxSpeed;

    // Start is called before the first frame update
    void Start()
    {}

    // Update is called once per frame
    void LateUpdate()
    {
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2((Time.time * parallaxSpeed), 0);
    }
}
