﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreTable : MonoBehaviour
{
    private Transform scoresTable;
    private Transform entryTemplate;

    private List<Transform> highscoreEntryTransformList;

    private void Awake()
    {
        scoresTable = gameObject.transform;
        entryTemplate = scoresTable.Find("ScoresEntryContainer");

        entryTemplate.gameObject.SetActive(false);

        //To avoid errors trying to work with an empty List
        Highscores dummy = new Highscores { highscoreEntryList = new List<HighscoreEntry>() { new HighscoreEntry { time = 0, coins = 0, lastEntry = false } } };
        string dummyJson = JsonUtility.ToJson(dummy);

        string jsonString = PlayerPrefs.GetString("highscoreTable", dummyJson);
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        highscoreEntryTransformList = new List<Transform>();
        foreach (HighscoreEntry highscoreEntry in highscores.highscoreEntryList)
        {
            if (highscoreEntry.time > 0) {
                CreateScoreEntryTransform(highscoreEntry, scoresTable, highscoreEntryTransformList);
            }
        }

    }

    private void CreateScoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 50f;

        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;
        string rankString;
        switch (rank)
        {
            default:
                rankString = rank + "TH";
                break;

            case 1:
                rankString = "1ST";
                break;
            case 2:
                rankString = "2ND";
                break;
            case 3:
                rankString = "3RD";
                break;
        }

        entryTransform.Find("PosText").GetComponent<Text>().text = rankString;

        float time = highscoreEntry.time;
        entryTransform.Find("TimeText").GetComponent<Text>().text = ShowTimeWithFormat(time); ;

        int coins = highscoreEntry.coins;
        entryTransform.Find("CoinsText").GetComponent<Text>().text = coins.ToString();

        entryTransform.Find("LastRecordMark").gameObject.SetActive(highscoreEntry.lastEntry); ;
        transformList.Add(entryTransform);
    }

    private string ShowTimeWithFormat(float time)
    {
        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = (time % 60).ToString("00");

        return string.Format("{0}:{1}", minutes, seconds);
    }

    public void ResetHighscoreTable()
    {
        List<HighscoreEntry> emptyList = new List<HighscoreEntry>();

        Highscores highscores = new Highscores {highscoreEntryList = emptyList};

        //Save empty Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();

        //PlayerPrefs.DeleteKey("highscoreTable");
    }

    /*
     * Needed this class to convert the list to an object and save it on a JSON file
     */
    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }

    /*
     * For a single Score Entry
     */
     [System.Serializable]
     private class HighscoreEntry
    {
        public float time;
        public int coins;
        public bool lastEntry;
    }
}
