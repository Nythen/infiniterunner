﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float xOffset = -1;

    void Update()
    {
        Vector3 targetPos = new Vector3(target.position.x + xOffset, target.position.y, target.position.z);
        LookAt(targetPos);
    }

    public void LookAt(Vector3 pos)
    {
        gameObject.transform.LookAt(pos);
    }
}
