﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    [SerializeField] private UIManager uiManager;

    private AddHighscore addHighscore;

    // Start is called before the first frame update
    void Start()
    {
        addHighscore = FindObjectOfType<AddHighscore>();
    }


    public void FinishGame()
    {
        //Save time/score
        addHighscore.AddHighscoreEntry(uiManager.GetCurrentTime(), uiManager.GetCurrentCoins());

        uiManager.EndGame();
    }


}
