﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxNoFloor : MonoBehaviour
{
    [SerializeField] private float parallaxSpeed = 0.05f;
    private float prevSpeed;

    private Coin[] coins;

    private void Start()
    {
        coins = GetComponentsInChildren<Coin>();
    }
    void LateUpdate()
    {
        transform.position += Vector3.right * parallaxSpeed;
    }

    public void StopParallax()
    {
        prevSpeed = parallaxSpeed;
        parallaxSpeed = 0;
    }

    public void StartParallax()
    {
        parallaxSpeed = prevSpeed;
    }

    //When the parallax is moved to be reused, the coins must be active again.
    public void activeAllCoins()
    {
        for (int i = 0; i < coins.Length; i++)
        {
            coins[i].gameObject.SetActive(true);
        }
    }
}
