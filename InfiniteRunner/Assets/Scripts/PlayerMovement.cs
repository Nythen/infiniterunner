﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float jumpForce = 250;
    
    private Rigidbody rb;
    [SerializeField] private Animator anim;

    private PlayerFoot playerFoot;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerFoot = GetComponentInChildren<PlayerFoot>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isPlayerOnTheFloor())
        {
            Jump();
        }
        else
        {
            anim.SetBool("isGrounded", true);
        }
    }

    public bool isPlayerOnTheFloor()
    {
        return playerFoot.isGrounded;
    }

    private void Jump()
    {
        rb.AddForce(new Vector3(0, jumpForce, 0));
        anim.SetBool("isGrounded", false);
    }

    public void Die()
    {
        anim.SetBool("Die", true);
    }

    public void StopRunning()
    {
        anim.SetBool("Pause", true);
    }

    public void ContinueRunning()
    {
        anim.SetBool("Pause", false);
    }
}
