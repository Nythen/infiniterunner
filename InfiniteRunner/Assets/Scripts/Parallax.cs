﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    private MovePackPlatforms movePlatforms;

    [SerializeField] private float parallaxSpeed = 0.05f;
    private float prevSpeed;

    void Start()
    {
        movePlatforms = GetComponentInParent<MovePackPlatforms>();
    }

    void LateUpdate()
    {
        transform.position += Vector3.right * parallaxSpeed;
    }

    //When the GO is invisible to the camera (and the Scene panel), it'll move to be reused.
    //Warning! - this only work with GO with a "Renderer" component in it.
    private void OnBecameInvisible()
    {
        Vector3 moveToPos = new Vector3(gameObject.transform.position.x - 60, 0);
        gameObject.transform.position = moveToPos;

        //move the enemy/platforms alongside with the floor
        movePlatforms.GetRandomPackPlatform(moveToPos);
    }

    public void StopParallax()
    {
        prevSpeed = parallaxSpeed;
        parallaxSpeed = 0;
    }

    public void StartParallax()
    {
        parallaxSpeed = prevSpeed;
    }
}
