﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public enum enemyType { LoseCoins, Killer };
    public enemyType currentEnemyType;

    public int coinsToLose;
    public GameObject coinsParticle;

    private Animator anim;
    private UIManager uiManager;
    private EndGame endGame;

    private void Start()
    {
        anim = GetComponent<Animator>();
        uiManager = FindObjectOfType<UIManager>();
        endGame = FindObjectOfType<EndGame>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            anim.SetBool("isPlayerClose", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            anim.SetBool("isPlayerClose", false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            hurtPlayer();
        }
    }

    private void hurtPlayer()
    {
        switch (currentEnemyType)
        {
            case enemyType.LoseCoins:
                anim.Play("Die");
                uiManager.LoseCoins(coinsToLose);
                Instantiate(coinsParticle, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y+1, gameObject.transform.position.z), Quaternion.identity);
                break;
            case enemyType.Killer:
                anim.SetBool("isHit", true);
                endGame.FinishGame();
                break;
        }
    }
}
