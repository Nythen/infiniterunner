﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddHighscore : MonoBehaviour
{
    public void AddHighscoreEntry(float time, int coins)
    {
        //Create HighscoreEntry
        HighscoreEntry highscoreEntry = new HighscoreEntry { time = time, coins = coins, lastEntry = true };

        //Load saved Highscores
        //To avoid errors trying to work with an empty List
        Highscores dummy = new Highscores { highscoreEntryList = new List<HighscoreEntry>() { new HighscoreEntry { time = 0, coins = 0, lastEntry = false } } };
        string dummyJson = JsonUtility.ToJson(dummy);

        string jsonString = PlayerPrefs.GetString("highscoreTable", dummyJson);
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        //Add new entry to Highscores only if breaks a record
        if (highscores.highscoreEntryList.Count < 15)
        {
            highscores.highscoreEntryList.Add(highscoreEntry);
        }
        else
        {
            //The list will be sorted, so if the latest element is lower I'll overwrite it
            HighscoreEntry lastEntry = highscores.highscoreEntryList[highscores.highscoreEntryList.Count-1];
            if (lastEntry.time < highscoreEntry.time)
            {
                highscores.highscoreEntryList.Remove(lastEntry);
                highscores.highscoreEntryList.Add(highscoreEntry);
            }
        }

        //Sort entry list by time
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for (int j = 0; j < highscores.highscoreEntryList.Count; j++)
            {
                if (highscores.highscoreEntryList[i].time > highscores.highscoreEntryList[j].time)
                {
                    //Swap if greater
                    HighscoreEntry aux = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = aux;
                }
            }
        }

        //make sure the last entry is the only one marked
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            if (highscores.highscoreEntryList[i] != highscoreEntry)
            {
                highscores.highscoreEntryList[i].lastEntry = false;
            }
        }

        //Save updated Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    /*
     * Needed this class to convert the list to an object and save it on a JSON file
     */
    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }

    /*
     * For a single Score Entry
     */
    [System.Serializable]
    private class HighscoreEntry
    {
        public float time;
        public int coins;
        public bool lastEntry;
    }
}
