﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePackPlatforms : MonoBehaviour
{
    public GameObject[] packPlatforms;
    private List<int> usedNumbers = new List<int>();

    private int index;
    private int actualPlatformIndex;
    private int prevPlatformIndex;

    void Start()
    {
        packPlatforms = GameObject.FindGameObjectsWithTag("packPlatforms");

        //randomize the platforms to add more varety on the gameplay and do it more scalable
        actualPlatformIndex = Random.Range(packPlatforms.Length/2, packPlatforms.Length);
        prevPlatformIndex = Random.Range(0, packPlatforms.Length/2);

        createRandomList();
    }

    public void GetRandomPackPlatform(Vector3 moveToPos)
    {
        GameObject actualPlatform = packPlatforms[usedNumbers[index]];

        actualPlatform.transform.position = moveToPos;
        actualPlatform.GetComponent<ParallaxNoFloor>().activeAllCoins();

        prevPlatformIndex = actualPlatformIndex;
        actualPlatformIndex = usedNumbers[index];

        print("index -> " + index);
        print("platform -> " + actualPlatformIndex);

        index++;

        //When all the packPlatforms has been used, create it again
        if (index == packPlatforms.Length)
        {
            createRandomList();
            index = 0;
        }
    }

    private void createRandomList()
    {
        List<int> newList = new List<int>();
        newList.Add(actualPlatformIndex);
        newList.Add(prevPlatformIndex);

        while (newList.Count < packPlatforms.Length)
        {
            int randomNumber = Random.Range(0, packPlatforms.Length);

            print(string.Format("{0} isn't contained? {1}.", randomNumber, !newList.Contains(randomNumber)));

            if (!newList.Contains(randomNumber))
            {
                newList.Add(randomNumber);
            }
        }

        //Reverse the list to keep the last platforms on the end (solve popping)
        newList.Reverse();

        usedNumbers = newList;
    }
}
